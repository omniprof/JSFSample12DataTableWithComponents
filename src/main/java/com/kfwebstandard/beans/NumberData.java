package com.kfwebstandard.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import javax.enterprise.context.RequestScoped;

import javax.inject.Named;

/**
 * Data for display in the table
 *
 * @author Ken Fogel
 */
@Named
@RequestScoped
public class NumberData implements Serializable {

    private final ArrayList<String> numbers;

    public NumberData() {
        this.numbers = new ArrayList<>(Arrays.asList(
                "1", "2", "3", "4", "5"));
    }

    public ArrayList<String> getNumbers() {
        return numbers;
    }

}
